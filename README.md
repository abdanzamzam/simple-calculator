# Simple Calculator

This is a simple calculator app built using Tailwind CSS and React.js.

Preview:

![Image](https://gitlab.com/abdanzamzam/simple-calculator/-/raw/main/Screenshot.png)
